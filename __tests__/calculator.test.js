// const calculator = require('../calculator');
import { calculator } from '../calculator';

describe('Add function', () => {
  test('Should return 2 if a = 1 and b = 1', () => {
    // Arrange
    const a = 1;
    const b = 1;

    // Act
    const result = calculator.add(a, b);

    // Assert
    expect(result).toEqual(2);
  });
});

describe('Minus function', () => {
  test('Should return 2 if a = 5 and b = 3', () => {
    // Arrange
    const a = 5;
    const b = 3;

    // Act
    const result = calculator.minus(a, b);

    // Assert
    expect(result).toEqual(2);
  });
});