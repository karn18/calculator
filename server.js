// const express = require('express');
import express from 'express';
const app = express();
const port = 3000;
const Sentry = require('@sentry/node');

Sentry.init({ 
  dsn: 'https://90f10ef6148c4549b8a4e5e780e0ed69@sentry.io/1812760',
  maxBreadcrumbs: 50,
  debug: true,
  environment: 'production',
  release: "calculator-rest-app@1.0.0"
});

Sentry.configureScope(function(scope) {
  scope.setUser({"email": "t.karn@hotmail.com", "username": "karn"});
  scope.setTag("version", "1.0.0");
});

app.use(Sentry.Handlers.requestHandler());

// All controllers should live here
app.get('/', function rootHandler(req, res) {
  res.end('Hello world!');
});

app.get('/test', (req, res) => {
  throw new Error("Error here");
});

app.get('/example', (req, res) => {
  Sentry.captureMessage('welcome to example');
  try {
    unknownFunction();
  } catch (error) {
    console.error(error);
    Sentry.captureException(error);
  }
  res.end("/example page");
});

// The error handler must be before any other error middleware and after all controllers
app.use(Sentry.Handlers.errorHandler());

// Optional fallthrough error handler
app.use(function onError(err, req, res, next) {
  // The error id is attached to `res.sentry` to be returned
  // and optionally displayed to the user for support.
  res.statusCode = 500;
  res.end(res.sentry + "\n");
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
