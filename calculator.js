function add(a, b) {
  return a + b;
}

function minus(a, b) {
  return a - b;
}

export const calculator = { add, minus }

// module.exports = calculator;
